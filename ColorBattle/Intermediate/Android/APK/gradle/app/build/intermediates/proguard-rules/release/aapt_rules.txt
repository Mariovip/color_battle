# view AndroidManifest.xml #generated:109
-keep class com.YourCompany.ColorBattle.AlarmReceiver { <init>(...); }

# view AndroidManifest.xml #generated:56
-keep class com.YourCompany.ColorBattle.DownloaderActivity { <init>(...); }

# view AndroidManifest.xml #generated:107
-keep class com.YourCompany.ColorBattle.OBBDownloaderService { <init>(...); }

# view AndroidManifest.xml #generated:44
-keep class com.epicgames.ue4.GameActivity { <init>(...); }

# view AndroidManifest.xml #generated:110
-keep class com.epicgames.ue4.LocalNotificationReceiver { <init>(...); }

# view AndroidManifest.xml #generated:111
-keep class com.epicgames.ue4.MulticastBroadcastReceiver { <init>(...); }

# view AndroidManifest.xml #generated:31
-keep class com.epicgames.ue4.SplashActivity { <init>(...); }

# view AndroidManifest.xml #generated:102
-keep class com.google.android.gms.ads.AdActivity { <init>(...); }

# view AndroidManifest.xml #generated:132
-keep class com.google.android.gms.auth.api.signin.RevocationBoundService { <init>(...); }

# view AndroidManifest.xml #generated:123
-keep class com.google.android.gms.auth.api.signin.internal.SignInHubActivity { <init>(...); }

# view AndroidManifest.xml #generated:137
-keep class com.google.android.gms.common.api.GoogleApiActivity { <init>(...); }

